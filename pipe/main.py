import os
import time
from urllib.parse import urlparse
import datetime
import pytz
import yaml

import boto3
from botocore.exceptions import ClientError

from bitbucket_pipes_toolkit import Pipe, get_logger


AWS_BASE_DOMAIN_NAME = 'amazonaws.com'
AWS_STACK_STATUS_CREATE_COMPLETE = 'CREATE_COMPLETE'
AWS_STACK_STATUS_UPDATE_COMPLETE = 'UPDATE_COMPLETE'
AWS_STACK_UNSUCCESS_STATUSES = (
    'CREATE_FAILED',
    'DELETE_FAILED',
    'ROLLBACK_FAILED',
    'UPDATE_ROLLBACK_FAILED',
    'ROLLBACK_COMPLETE',
    'ROLLBACK_IN_PROGRESS',
    'UPDATE_ROLLBACK_COMPLETE',
    'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS',
    'UPDATE_ROLLBACK_FAILED',
    'UPDATE_ROLLBACK_IN_PROGRESS',
)
MSG_AWS_BOTO_ERROR = 'Failed to create boto3 client.'
MSG_AWS_CFN_CREATE_FAILED = 'Failed to create the stack.'
MSG_AWS_CFN_UPDATE_FAILED = 'Failed to update the stack.'

WAIT_INTERVAL_DEFAULT = 30  # sec
WAIT_MAX_ATTEMPTS_DEFAULT = 120
TEMPLATE_SIZE_LIMIT = 51200  # bytes


logger = get_logger()

schema = {
    "AWS_ACCESS_KEY_ID": {
        "type": "string",
        "required": True
    },
    "AWS_SECRET_ACCESS_KEY": {
        "type": "string",
        "required": True
    },
    "AWS_DEFAULT_REGION": {
        "type": "string",
        "required": True
    },
    "STACK_NAME": {
        "type": "string",
        "required": True
    },
    "TEMPLATE": {
        "type": "string",
        "required": True,
    },
    "STACK_PARAMETERS": {
        "empty": False,
        "required": False,
        "schema": {
            "type": "dict",
            "schema": {
                "ParameterKey": {
                    "type": "string",
                    "required": True
                },
                "ParameterValue": {
                    "type": "string",
                    "required": True
                }
            }
        }
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    },
    "WAIT": {
        "type": "boolean",
        "default": False
    },
    "WAIT_INTERVAL": {
        "type": "number",
        "default": WAIT_INTERVAL_DEFAULT
    },
    "CAPABILITIES": {
        "type": "list",
        "required": False,
    },
    "TAGS": {
        "empty": False,
        "required": False,
        "schema": {
            "type": "dict",
            "schema": {
                "Key": {
                    "type": "string",
                    "required": True
                },
                "Value": {
                    "type": "string",
                    "required": True
                }
            }
        }
    },
    "WITH_DEFAULT_TAGS": {
        "type": "boolean",
        "default": True
    }
}

utc = pytz.UTC


class CloudformationDeployPipe(Pipe):

    def __init__(self, pipe_metadata=None, schema=None, env=None, client=None, check_for_newer_version=False):
        super().__init__(pipe_metadata=pipe_metadata, schema=schema, env=env, check_for_newer_version=check_for_newer_version)
        self.client = client
        self.action = None

    def get_client(self):
        try:
            return boto3.client(
                'cloudformation',
                region_name=self.get_variable('AWS_DEFAULT_REGION'))
        except ClientError as error:
            self.fail(f'{MSG_AWS_BOTO_ERROR}\n{error}')

    def validate_template(self, **kwargs):
        logger.info(f'Validating the template.')

        try:
            self.client.validate_template(**kwargs)
        except ClientError as error:
            self.fail(f'Template validation failed.\n{error}')

    def update_stack(self, **kwargs):
        '''Update cloudformation stack

            # Amazon API Gateway
            # A response code of 2xx indicates the operation was successful.
            # Other error codes indicate either a client error (4xx) or a server error (5xx).
            # https://docs.aws.amazon.com/apigateway/api-reference/handling-errors/
        '''

        logger.info(f'Updating the stack for {self.get_variable("STACK_NAME")}.')

        self.action = 'updated'

        try:
            return self.client.update_stack(**kwargs)
        except ClientError as error:
            if 'No updates are to be performed' in str(error):
                logger.warning(f'No updates are to be performed for stack {self.get_variable("STACK_NAME")}.')
                return {}
            elif 'Stack' in str(error) and 'does not exist' in str(error):
                logger.warning(f'Stack {self.get_variable("STACK_NAME")} does not exist.')
                return self.create_stack(**kwargs)
            else:
                self.fail(f'{MSG_AWS_CFN_UPDATE_FAILED}\n{error}{self.get_stack_link()}')

    def create_stack(self, **kwargs):
        '''Create cloudformation stack

            # Amazon API Gateway
            # A response code of 2xx indicates the operation was successful.
            # Other error codes indicate either a client error (4xx) or a server error (5xx).
            # https://docs.aws.amazon.com/apigateway/api-reference/handling-errors/
        '''

        logger.info(f'Creating the stack for {self.get_variable("STACK_NAME")}.')

        self.action = 'created'

        try:
            response = self.client.create_stack(**kwargs)
            return response
        except ClientError as error:
            self.fail(f'{MSG_AWS_CFN_CREATE_FAILED}\n{error}{self.get_stack_link()}')

    def get_stack(self, stack_name):
        try:
            response = self.client.describe_stacks(StackName=stack_name)
        except ClientError as error:
            logger.error(f'Failed to get information about stack {stack_name}.\n{error}')
            return None

        # no Stacks key and empty list
        if not response.get('Stacks'):
            return None

        return response['Stacks'][0]

    def get_stack_link(self, stack_id=None):
        link = ''

        if not stack_id:
            # try to get StackId
            stack = self.get_stack(self.get_variable("STACK_NAME"))
            stack_id = stack.get('StackId') if stack else None

        if stack_id:
            link = (
                f'\nYou can check you stack here: '
                f'https://console.aws.amazon.com/cloudformation/home?region={self.get_variable("AWS_DEFAULT_REGION")}#/stack/detail?stackId={stack_id}.'
            )

        return link

    def is_success_updated_stack(self, stack):

        status = stack.get('StackStatus')
        stack_id = stack.get('StackId')

        if status is None:
            return False

        if status in [AWS_STACK_STATUS_UPDATE_COMPLETE, AWS_STACK_STATUS_CREATE_COMPLETE]:
            return True

        if status in AWS_STACK_UNSUCCESS_STATUSES:
            self.fail(
                f'{MSG_AWS_CFN_UPDATE_FAILED} Status {status}.\n'
                f'Stack name {self.get_variable("STACK_NAME")}. You can check you stack here: \n'
                f'https://console.aws.amazon.com/cloudformation/home?region={self.get_variable("AWS_DEFAULT_REGION")}#/stack/detail?stackId={stack_id}.'
            )

        return False

    def get_template_file_size(self, template_name):
        try:
            return os.path.getsize(template_name)
        except FileNotFoundError:
            self.fail(f'Not able to find the file {template_name} in your repository.\n')
        except OSError as error:
            self.fail(f'Problem with the template file.\n {error}')

    def get_template_parameter(self):
        """Get template content or URL AWS S3 location parameter.

        Returns:
            dict with one key
                or 'TemplateURL' : 'URL to file on AWS S3'
                or 'TemplateBody' : 'Content of templatate'
        """
        param = {}

        template = urlparse(self.get_variable("TEMPLATE"))
        if template.scheme in ['s3']:
            self.fail(f'Required the URL that must point to the template file on Amazon S3 bucket. But AWS S3 path provided {self.get_variable("TEMPLATE")}.')

        if template.scheme in ['https', 'http'] \
            and template.netloc.endswith(AWS_BASE_DOMAIN_NAME) \
            and 's3' in template.netloc:
            # AWS S3 URL
            logger.info(f'Using stack template from {self.get_variable("TEMPLATE")} for deploy.')
            param['TemplateURL'] = self.get_variable("TEMPLATE")
        else:
            # use local file
            cfn_template_file_name = self.get_variable("TEMPLATE")
            logger.info(f'Using stack template from {cfn_template_file_name} for deploy.')

            template_file_size = self.get_template_file_size(cfn_template_file_name)
            if template_file_size >= TEMPLATE_SIZE_LIMIT:
                self.fail(
                    f'Size of the local template file {cfn_template_file_name} more than {TEMPLATE_SIZE_LIMIT} bytes allowed by AWS Cloudformation.\n'
                    f'CloudFormation templates greater than {TEMPLATE_SIZE_LIMIT} bytes must be deployed from an S3 bucket.\n'
                    f'More details your can find on the https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html.'
                )

            # get template's content
            try:
                with open(cfn_template_file_name, 'r') as f:
                    param['TemplateBody'] = f.read()
            except FileNotFoundError:
                self.fail(f'Not able to find the file {cfn_template_file_name} in your repository.\n')
            except OSError as error:
                self.fail(f'Problem with the template file.\n {error}')

        return param

    def get_stack_events(self, stack):

        events = []
        next_token = None

        while True:
            try:
                if next_token is None:
                    response = self.client.describe_stack_events(StackName=stack['StackId'])
                else:
                    response = self.client.describe_stack_events(StackName=stack['StackId'], NextToken=next_token)
            except ClientError as error:
                logger.error(f'Failed to get stack events from stack {stack["StackId"]}.\n{error}')
                return []

            next_token = response.get('NextToken')
            events.extend(response['StackEvents'])
            if next_token is None:
                return events

    def get_tags(self):
        tags = []

        with_default_tags = self.get_variable('WITH_DEFAULT_TAGS')
        if with_default_tags:
            tags.extend([
                {
                    'Key': 'BITBUCKET_REPO_URL',
                    'Value': f"https://bitbucket.org/{os.getenv('BITBUCKET_REPO_FULL_NAME')}"},

                {
                    'Key': 'BITBUCKET_COMMIT',
                    'Value': os.getenv('BITBUCKET_COMMIT'),
                },
                {
                    'Key': 'BITBUCKET_BUILD_NUMBER',
                    'Value': os.getenv('BITBUCKET_BUILD_NUMBER')
                }
            ])

            branch = os.getenv('BITBUCKET_BRANCH')
            if branch:
                tags.append(
                    {
                        'Key': 'BITBUCKET_BRANCH',
                        'Value': branch,
                    }
                )

            env = os.getenv('BITBUCKET_DEPLOYMENT_ENVIRONMENT')
            if env:
                tags.append({'Key': 'BITBUCKET_DEPLOYMENT_ENVIRONMENT', 'Value': env})

        if 'TAGS' in self.variables:
            defined_tags = list(self.get_variable('TAGS'))
            for defined_tag in defined_tags:
                found = False
                for tag in tags:
                    if tag['Key'] == defined_tag['Key']:
                        tag['Value'] = defined_tag['Value']
                        found = True
                if not found:
                    tags.append(defined_tag.copy())

        return tags

    def run(self):
        super().run()

        self.client = self.get_client()

        cfn_params = {}
        cfn_params.update(self.get_template_parameter())
        self.validate_template(**cfn_params)

        cfn_params['StackName'] = self.get_variable('STACK_NAME')

        if 'STACK_PARAMETERS' in self.variables:
            cfn_params['Parameters'] = self.get_variable('STACK_PARAMETERS')

        if 'CAPABILITIES' in self.variables:
            cfn_params['Capabilities'] = list(self.get_variable('CAPABILITIES'))

        tags = self.get_tags()
        if len(tags) > 0:
            cfn_params['Tags'] = tags

        update_start_time = datetime.datetime.utcnow()
        response = self.update_stack(**cfn_params)

        if self.get_variable('WAIT'):
            logger.info('Waiting for update COMPLETE status...')

            attempts_counter = 0
            seen_events_ids = set()

            while attempts_counter <= WAIT_MAX_ATTEMPTS_DEFAULT:
                attempts_counter += 1

                stack = self.get_stack(self.get_variable('STACK_NAME'))

                if stack is None:
                    logger.error(f'Failed to get information about stack {self.get_variable("STACK_NAME")}.')

                events = self.get_stack_events(stack)
                for event in events:
                    if event['Timestamp'].replace(tzinfo=utc) > update_start_time.replace(tzinfo=utc) and event['EventId'] not in seen_events_ids:
                        seen_events_ids.add(event['EventId'])
                        logger.info(
                            f"{event['Timestamp']} {event['ResourceStatus']} {event['ResourceType']} {event['LogicalResourceId']} "
                        )

                if self.is_success_updated_stack(stack):
                    break
                time.sleep(self.get_variable('WAIT_INTERVAL'))
            else:
                self.fail(f'Max attempts exceeded during wait for stack {self.get_variable("STACK_NAME")}.')

        msg = f'Successfully {self.action} the {self.get_variable("STACK_NAME")} stack.'
        if response.get("StackId"):
            msg = ''.join([msg, self.get_stack_link(response["StackId"])])

        self.success(msg)


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/usr/bin/pipe.yml', 'r'))
    pipe = CloudformationDeployPipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
